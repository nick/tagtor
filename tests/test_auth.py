"""
Auth tests
"""
import pytest
import requests

def test_index(client):
    username = 'metrics'
    password = 'password'

    response = client.get('/auth', environ_base={'HTTP_AUTHORIZATION': 'Basic bWV0cmljczpwYXNzd29yZA=='})

    assert b"Hello metrics" in response.data
