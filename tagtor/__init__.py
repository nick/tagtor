import base64
import os

from flask import (
    Flask, render_template, request
)

from tagtor.vm import get_time_series


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('settings.cfg', silent=True)
    else:
        # load the test config if passed in
        app.config.from_pyfile('test_settings.cfg', silent=True)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/')
    def index():

        params_bw_hist = {
            "query": "sum(" +
                     "sum(" +
                     "sum_over_time(write_bandwidth_history{" +
                     "node=\"relay\"}[24h]))," +
                     "sum(" +
                     "sum_over_time(" +
                     "read_bandwidth_history{" +
                     "node=\"relay\"}[24h])))*8/2e12",
            "start": "-30d",
            "end": "-5d",
            "step": "24h",
            "nocache": "1"
        }
        time_vector_bw_hist = []
        values_vector_bw_hist = []
        time_vector_bw_hist, values_vector_bw_hist = get_time_series(params_bw_hist)

        params_bw_adv = {
            "query": "sum(" +
                     "sum_over_time(" +
                     "(min" +
                     "(bw_file_desc_bw_avg, " +
                     "bw_file_desc_bw_bur, bw_file_desc_bw_obs_last)" +
                     " by (fingerprint, time)" +
                     "))[24h])*8/10e8",
            "start": "-30d",
            "end": "-5d",
            "step": "24h",
            "nocache": "1"
        }
        time_vector_bw_adv = []
        values_vector_bw_adv = []
        time_vector_bw_adv, values_vector_bw_adv = get_time_series(params_bw_adv)

        params_relays = {
            "query": "sum_over_time(count(network_fraction) by (day))[24h]",
            "start": "-30d",
            "end": "-5d",
            "step": "24h",
            "nocache": "1"
        }
        time_vector_relays = []
        values_vector_relays = []
        time_vector_relays, values_vector_relays = get_time_series(params_relays)

        params_bridges = {
            "query": "sum_over_time(count(bridge_bandwidth) by (day))[24h]",
            "start": "-30d",
            "end": "-5d",
            "step": "24h",
            "nocache": "1"
        }
        time_vector_bridges = []
        values_vector_bridges = []
        time_vector_bridges, values_vector_bridges = get_time_series(params_bridges)

        auth_header = str.encode(request.headers['Authorization'].split('Basic ')[1])

        username = base64.b64decode(auth_header).decode("utf-8").split(':')[0]

        context = {
          "title": "Tor Metrics",
          "consumed_bw": values_vector_bw_hist,
          "time_bw": time_vector_bw_hist,
          "adv_bw": values_vector_bw_adv,
          "adv_time": time_vector_bw_adv,
          "relays": values_vector_relays,
          "relays_time": time_vector_relays,
          "bridges": values_vector_bridges,
          "bridges_time": time_vector_bridges,
          "username": username
        }

        return render_template('index.html', **context)

    from . import auth, router

    app.register_blueprint(router.bp)
    app.add_url_rule('/routers', endpoint='index')

    app.register_blueprint(auth.bp)
    app.add_url_rule('/auth', endpoint='index')

    return app
