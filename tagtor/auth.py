"""
Auth logic
"""

import base64

from flask import (
    Blueprint, render_template, request
)

bp = Blueprint('auth', __name__)

@bp.route('/auth')
def index():
    auth_header = str.encode(request.headers['Authorization'].split('Basic ')[1])
    username = base64.b64decode(auth_header).decode("utf-8").split(':')[0]
    context = {
        "title": "Tor nodes",
        "username": username,
    }
    return render_template('auth/index.html', **context)
